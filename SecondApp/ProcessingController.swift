//
//  ProcessingController.swift
//  SecondApp
//
//  Created by zenon on 28.05.19.
//  Copyright © 2019 zenon. All rights reserved.
//

import UIKit
import Vision
import ImageIO
import CoreML

class ProcessingController: NSObject {
    var inputImage: CIImage!
    var processingOrientation: CGImagePropertyOrientation!;
    var rectangleDetectionCompletionHandler: ((VNRectangleObservation?, CIImage?, Error?) -> Void)?
    var imageClassificationCompletionHandler: ((VNClassificationObservation?, Error?) -> Void)?
    
    lazy var rectanglesRequest: VNDetectRectanglesRequest = {
        return VNDetectRectanglesRequest(completionHandler: self.handleRectangleDetectionRequest)
    }()
    
    lazy var a3rectanglesRequest: VNDetectRectanglesRequest = {
        let detectRequest = VNDetectRectanglesRequest(completionHandler: self.handleRectangleDetectionRequest)
        //detectRequest.minimumAspectRatio = 0.3
        //detectRequest.minimumConfidence = 0.5
        return detectRequest;
    }()
    
    lazy var classificationRequest: VNCoreMLRequest = {
        let model = try! VNCoreMLModel(for: inceptionV3().model);
        let request = VNCoreMLRequest(model: model, completionHandler: { [weak self] request, error in
            self?.handleImageClassificationRequest(for: request, error: error)
        })
        request.imageCropAndScaleOption = .centerCrop
        return request
    }()
    
    func performA3PaperDetection(image: UIImage, completionHandler: @escaping (VNRectangleObservation?, CIImage?, Error?) -> Void){
        self.performRectangleDetection(image: image, completionHandler: completionHandler, request: self.a3rectanglesRequest)
    }
    
    func performRectangleDetection(image: UIImage, completionHandler: @escaping (VNRectangleObservation?, CIImage?, Error?) -> Void, request: VNDetectRectanglesRequest? = nil){
        self.rectangleDetectionCompletionHandler = completionHandler;
        let request = request ?? self.rectanglesRequest
        
        guard let ciImage = CIImage(image: image)
            else { fatalError("can't create CIImage from UIImage") }
        
        let orientation = CGImagePropertyOrientation(image.imageOrientation)
        inputImage = ciImage.oriented(forExifOrientation: Int32(orientation.rawValue))
        processingOrientation = CGImagePropertyOrientation(rawValue: UInt32(Int32(orientation.rawValue)))
        
        let handler = VNImageRequestHandler(ciImage: ciImage, orientation: processingOrientation)
        DispatchQueue.global(qos: .userInteractive).async {
            do {
                try handler.perform([request])
            } catch {
                print(error)
            }
        }
    }
    
    func handleRectangleDetectionRequest(request: VNRequest, error: Error?) {
        guard let observations = request.results as? [VNRectangleObservation]
            else { fatalError("unexpected result type from VNDetectRectanglesRequest") }
        guard let detectedRectangle = observations.first else {
            DispatchQueue.main.async {
                self.rectangleDetectionCompletionHandler!(nil,nil,error)
            }
            return
        }
        let imageSize = inputImage.extent.size
        
        // Verify detected rectangle is valid.
        let boundingBox = detectedRectangle.boundingBox.scaled(to: imageSize)
        guard inputImage.extent.contains(boundingBox)
            else { print("invalid detected rectangle"); return }
        
        
        
        // Rectify the detected image and reduce it to inverted grayscale for applying model.
        let topLeft = detectedRectangle.topLeft.scaled(to: imageSize)
        let topRight = detectedRectangle.topRight.scaled(to: imageSize)
        let bottomLeft = detectedRectangle.bottomLeft.scaled(to: imageSize)
        let bottomRight = detectedRectangle.bottomRight.scaled(to: imageSize)
        let correctedImage = inputImage
            .cropped(to: boundingBox)
            .applyingFilter("CIPerspectiveCorrection", parameters: [
                "inputTopLeft": CIVector(cgPoint: topLeft),
                "inputTopRight": CIVector(cgPoint: topRight),
                "inputBottomLeft": CIVector(cgPoint: bottomLeft),
                "inputBottomRight": CIVector(cgPoint: bottomRight)
                ])
            .applyingFilter("CIColorControls", parameters: [
                kCIInputSaturationKey: 0,
                kCIInputContrastKey: 32
                ])
            .applyingFilter("CIColorInvert", parameters: [:])
        
        DispatchQueue.main.async {
            self.rectangleDetectionCompletionHandler!(detectedRectangle,correctedImage,error)
        }
    }
    
    func performImageClassification(image: CIImage, completionHandler: @escaping (VNClassificationObservation?, Error?) -> Void, request: VNCoreMLRequest? = nil){
        self.imageClassificationCompletionHandler = completionHandler;
        let request = request ?? self.classificationRequest
        
        //CoreML processing
        DispatchQueue.global(qos: .userInitiated).async {
            let handler = VNImageRequestHandler(ciImage: image, orientation: self.processingOrientation)
            do {
                try handler.perform([self.classificationRequest])
            } catch {
                /*
                 This handler catches general image processing errors. The `classificationRequest`'s
                 completion handler `processClassifications(_:error:)` catches errors specific
                 to processing that request.
                 */
                print("Failed to perform classification.\n\(error.localizedDescription)")
            }
        }
        
    }
    
    func handleImageClassificationRequest(for request: VNRequest, error: Error?) {
        guard let results = request.results else {
            DispatchQueue.main.async {
                self.imageClassificationCompletionHandler!(nil, error)
            }
            return
        }
        let classifications = results as! [VNClassificationObservation]
        guard let classification = classifications.first else {
            DispatchQueue.main.async {
                self.imageClassificationCompletionHandler!(nil, error)
            }
            return
        }
        
        DispatchQueue.main.async {
            self.imageClassificationCompletionHandler!(classification, error)
        }
    }
}
