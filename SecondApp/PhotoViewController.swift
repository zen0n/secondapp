//
//  PhotoViewController.swift
//  SecondApp
//
//  Created by zenon on 2.04.19.
//  Copyright © 2019 zenon. All rights reserved.
//

import UIKit
import Vision

class PhotoViewController: UIViewController {
    let logicController = ProcessingController()
    
    @IBOutlet var capturedImageView: UIImageView!
    @IBOutlet var a5TemplateImageView: UIImageView!
    @IBOutlet var imageSimilarity: UILabel!
    
    var capturedImage: UIImage!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let a5template = UIImage(named: "a5template");
        //self.a5TemplateImageView.image = a5template;
        //self.a5TemplateImageView.contentMode = .scaleAspectFit;
        
        if capturedImage != nil {
            self.capturedImageView.image = capturedImage;
            
//            logicController.performA3PaperDetection(image: capturedImage, completionHandler: {(rect, error) in
                self.logicController.performRectangleDetection(image: capturedImage, completionHandler: {(rect, image, error) in
                    guard let rect = rect else {
                        print(error ?? "Rectangle not detected")
                        return
                    }
                    self.drawObservedRectangle(rect);
                    self.imageSimilarity.text = "Performing image classification"//String(format:"%d %% confidence", maxVal);
                    
                    self.logicController.performImageClassification(image: image!, completionHandler: {(classification, error) in
                        guard let classification = classification else {
                            print(error ?? "Failed to perform classification")
                            return
                        }
                        self.imageSimilarity.text = String(format:"%.2f %% confidence", (classification.confidence*100));
                    })
                    /*
                    let a = self.capturedImageView.layer.bounds;
                    let blueHighlightedLayer = CALayer()
                    blueHighlightedLayer.frame = rect
                    blueHighlightedLayer.borderWidth = 3.0
                    blueHighlightedLayer.borderColor = UIColor.blue.cgColor
                    self.capturedImageView.layer.addSublayer(blueHighlightedLayer)
                     */
                    //self.capturedImageView.image = self.capturedImage.cropped(boundingBox: rect);
                    //self.capturedImageView.setNeedsDisplay();
                    //self.capturedImageView.contentMode = .redraw;
                    //self.capturedImageView.contentMode = ;
                    //self.capturedImageView.setNeedsDisplay();
                })
            self.capturedImageView.contentMode = .scaleAspectFit;
            self.capturedImageView.setNeedsDisplay();
        }
       
        /*
        if capturedImage != nil {
            self.detector = self.prepareRectangleDetector();
            guard let cImage = CIImage(image: capturedImage, options: [.applyOrientationProperty:true]) else { return }
            let dImage = self.performRectangleDetection(image: cImage);
            if(dImage != nil){
                self.capturedImageView.image =  UIImage(ciImage: dImage!, scale: capturedImage.scale, orientation:capturedImage.imageOrientation)
            }
            else{
                self.capturedImageView.image = capturedImage;
            }
            
            self.capturedImageView.contentMode = .scaleAspectFit;
            self.capturedImageView.setNeedsDisplay();
            

            //let openCVWrapper = OpenCVWrapper();
            //let maxVal = openCVWrapper.processImages(capturedImage, template:a5template!);
            //self.imageSimilarity.text = String(format:"%d %%", maxVal);
        }
         */

        // Do any additional setup after loading the view.
    }
    
    private func drawObservedRectangle(_ rectangle: VNRectangleObservation) {
        guard let targetSize = capturedImageView?.contentClippingRect.size else {
            print("targetImageView doesn't exist")
            return
        }
        
        let rectangleShape = CAShapeLayer()
        rectangleShape.opacity = 0.5
        rectangleShape.lineWidth = 5
        rectangleShape.lineJoin = CAShapeLayerLineJoin.round
        rectangleShape.strokeColor = UIColor.blue.cgColor
        rectangleShape.fillColor = UIColor.blue.withAlphaComponent(0.6).cgColor
        
        let rectanglePath = UIBezierPath()
        rectanglePath.move(to: rectangle.topLeft.scaled(to: targetSize))
        rectanglePath.addLine(to: rectangle.topRight.scaled(to: targetSize))
        rectanglePath.addLine(to: rectangle.bottomRight.scaled(to: targetSize))
        rectanglePath.addLine(to: rectangle.bottomLeft.scaled(to: targetSize))
        rectanglePath.close()
        
        rectangleShape.path = rectanglePath.cgPath
        capturedImageView?.layer.addSublayer(rectangleShape)
        self.capturedImageView.contentMode = .scaleAspectFit;
        self.capturedImageView.setNeedsDisplay();
    }
    
    
    var detector: CIDetector?
    func prepareRectangleDetector() -> CIDetector {
        return CIDetector(
            ofType: CIDetectorTypeRectangle,
            context: nil,
            options: [CIDetectorAccuracy: CIDetectorAccuracyHigh, CIDetectorAspectRatio: 1.0]
            )!
    }
    
    func performRectangleDetection(image: CIImage) -> CIImage? {
        var resultImage: CIImage?
        if let detector = detector {
            // Get the detections
            let features = detector.features(in: image)
            for feature in features as! [CIRectangleFeature] {
                resultImage = drawHighlightOverlayForPoints(image, topLeft: feature.topLeft, topRight: feature.topRight,
                                                            bottomLeft: feature.bottomLeft, bottomRight: feature.bottomRight)
            }
        }
        return resultImage
    }
    
    func drawHighlightOverlayForPoints(_ image: CIImage, topLeft: CGPoint, topRight: CGPoint,
                                       bottomLeft: CGPoint, bottomRight: CGPoint) -> CIImage {
        var overlay = CIImage(color: CIColor(red: 1.0, green: 0, blue: 0, alpha: 0.5))
        overlay = overlay.cropped(to: image.extent)
        overlay = overlay.applyingFilter("CIPerspectiveTransformWithExtent",
                                         parameters: [
                                            "inputExtent": CIVector(cgRect: image.extent),
                                            "inputTopLeft": CIVector(cgPoint: topLeft),
                                            "inputTopRight": CIVector(cgPoint: topRight),
                                            "inputBottomLeft": CIVector(cgPoint: bottomLeft),
                                            "inputBottomRight": CIVector(cgPoint: bottomRight)
            ])
        return overlay.composited(over: image)
    }
    
    func cropBusinessCardForPoints(image: CIImage, topLeft: CGPoint, topRight: CGPoint, bottomLeft: CGPoint, bottomRight: CGPoint) -> CIImage {
        
        var businessCard: CIImage
        businessCard = image.applyingFilter(
            "CIPerspectiveTransformWithExtent",
            parameters: [
                "inputExtent": CIVector(cgRect: image.extent),
                "inputTopLeft": CIVector(cgPoint: topLeft),
                "inputTopRight": CIVector(cgPoint: topRight),
                "inputBottomLeft": CIVector(cgPoint: bottomLeft),
                "inputBottomRight": CIVector(cgPoint: bottomRight)])
        businessCard = image.cropped(to: businessCard.extent)
        
        return businessCard
    }
    
    func imageOrientationToExif(image: UIImage) -> Int32 {
        switch image.imageOrientation {
        case UIImage.Orientation.up:
            return 1;
        case UIImage.Orientation.down:
            return 3;
        case UIImage.Orientation.left:
            return 8;
        case UIImage.Orientation.right:
            return 6;
        case UIImage.Orientation.upMirrored:
            return 2;
        case UIImage.Orientation.downMirrored:
            return 4;
        case UIImage.Orientation.leftMirrored:
            return 5;
        case UIImage.Orientation.rightMirrored:
            return 7;
        }
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

